import React from 'react';
import axios from 'axios';
import { Formik } from 'formik';
import { useHistory } from 'react-router';
import * as yup from 'yup';
import NumberFormat from 'react-number-format';
import './Form.css';
import { message } from 'antd';

const RegistrationList = () => {
  const hist = useHistory();
  const validationSchema = yup.object().shape({
    firstName: yup.string().typeError('Вводите строкой').required('Поле обязательно для ввода'),
    lastName: yup.string().typeError('Вводите строкой').required('Поле обязательно для ввода'),
    login: yup
      .string()
      .min(4, 'Login should be 4 chars minimum.')
      .max(8, 'Login should be 8 chars maximum.')
      .matches(/[a-zA-Z]/, 'Login can only contain Latin letters.')
      .typeError('Вводите строкой')
      .required('Поле обязательно для ввода'),
    password: yup
      .string()
      .min(8, 'Password is too short - should be 8 chars minimum.')
      .matches(/[a-zA-Z]/, 'Password can only contain Latin letters.')
      .required('Поле обязательно для ввода'),
    email: yup.string().email('Введите корректный email').required('Поле обязательно для ввода'),
  });
  const clearCart = async (data) => {
    const body = JSON.stringify(data);
    await axios({
      method: 'post',
      url: `/api/customers`,
      headers: { 'Content-Type': 'application/json' },
      data: body,
    })
      .then(function (response) {
        if (response.status === 200) hist.push('/login');
        message.success('Спасибо за регистрацию!', 2);
      })
      .catch(function (error) {
        message.error(`${error.response.data.message}`, 2);
      });
  };
  return (
    <div className="form-wrapper">
      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          login: '',
          telephone: '',
          email: '',
          password: '',
        }}
        validateOnBlur
        onSubmit={(values) => {
          clearCart(values);
        }}
        validationSchema={validationSchema}
      >
        {({ values, errors, touched, handleChange, handleBlur, isValid, handleSubmit, dirty }) => (
          <div className="form-conteiner">
            <p className="title">Регистрация</p>
            <p>
              <input
                className="input-form"
                placeholder="Введите Ваше имя"
                type="text"
                name="firstName"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.firstName}
              />
            </p>
            {touched.firstName && errors.firstName && (
              <p className="error-massage">{errors.firstName}</p>
            )}
            <p>
              <input
                className="input-form"
                placeholder="Введите Вашу фамилию"
                type="text"
                name="lastName"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.lastName}
              />
            </p>
            {touched.lastName && errors.lastName && (
              <p className="error-massage">{errors.lastName}</p>
            )}
            <p>
              <input
                className="input-form"
                placeholder="Введите ник"
                type="text"
                name="login"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.login}
              />
            </p>
            {touched.login && errors.login && <p className="error-massage">{errors.login}</p>}
            <p>
              <input
                className="input-form"
                placeholder="Введите пароль"
                type="text"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
            </p>
            {touched.password && errors.password && (
              <p className="error-massage">{errors.password}</p>
            )}
            <p>
              <input
                className="input-form"
                placeholder="Введите Ваш email"
                type="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
            </p>
            {touched.email && errors.email && <p className="error-massage">{errors.email}</p>}
            <p>
              <NumberFormat
                format="+38##########"
                mask="_"
                className="input-form"
                placeholder="Введите Ваш номер телефона"
                type="telephone"
                name="telephone"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.telephone}
              />
            </p>
            {touched.telephone && errors.telephone && (
              <p className="error-massage">{errors.telephone}</p>
            )}
            <button
              disabled={!isValid && !dirty}
              onClick={handleSubmit}
              type="submit"
              className="button-submit"
              data-testid="button-submit"
            >
              Отправить
            </button>
          </div>
        )}
      </Formik>
    </div>
  );
};

export default RegistrationList;
