import React, { useState } from 'react';
import { Select } from 'antd';
import CategoriesList from '../../components/CategoriesList/CategoriesList';
import ProductsList from '../../components/ProductsList/ProductsList';

const Filter = () => {
  const [sortPrice, setSortPrice] = useState('');
  const { Option } = Select;
  const handleChange = (value) => {
    console.log(`selected ${value}`);
    setSortPrice(value);
  };
  return (
    <div className="wrapper">
      <CategoriesList />
      <Select defaultValue="умолчанию" style={{ width: 120 }} onChange={handleChange}>
        <Option value="-currentPrice">от дорогих ...</Option>
        <Option value="+currentPrice">от дешевых ....</Option>
        <Option value=" ">по умолчанию</Option>
      </Select>
      <ProductsList sortPrice={sortPrice} />
    </div>
  );
};

export default Filter;
