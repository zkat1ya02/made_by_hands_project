import React from 'react';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import * as yup from 'yup';
import axios from 'axios';
import { message } from 'antd';
import '../RegistrationList/Form.css';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router';
import actions from '../../store/token/actions';

const LoginList = ({ dispatch }) => {
  const hist = useHistory();
  const validationSchema = yup.object().shape({
    loginOrEmail: yup.string().typeError('Вводите строкой').required('Поле обязательно для ввода'),
    password: yup
      .string()
      .min(8, 'Password is too short - should be 8 chars minimum.')
      .matches(/[a-zA-Z]/, 'Password can only contain Latin letters.')
      .required('Поле обязательно для ввода'),
  });
  const loginUser = async (data) => {
    const body = JSON.stringify(data);
    await axios({
      method: 'post',
      url: `/api/customers/login`,
      headers: { 'Content-Type': 'application/json' },
      data: body,
    })
      .then(function (response) {
        dispatch(actions.setToken(response.data.token));
        localStorage.setItem('token', JSON.stringify(response.data.token));
        if (response.status === 200) hist.push('/');
        message.success('Добро пожаловать!', 2);
      })
      .catch(function (error) {
        console.log(error);
        message.error('Что-то пошло не так!', 2);
      });
  };
  return (
    <div className="form-wrapper">
      <Formik
        initialValues={{
          loginOrEmail: '',
          password: '',
        }}
        validateOnBlur
        onSubmit={(values) => {
          loginUser(values);
        }}
        validationSchema={validationSchema}
      >
        {({ values, errors, touched, handleChange, handleBlur, isValid, handleSubmit, dirty }) => (
          <div className="form-conteiner">
            <p className="title">Авторизация</p>
            <p>
              <input
                className="input-form"
                placeholder="Введите ник"
                type="text"
                name="loginOrEmail"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.loginOrEmail}
              />
            </p>
            {touched.loginOrEmail && errors.loginOrEmail && (
              <p className="error-massage">{errors.loginOrEmail}</p>
            )}
            <p>
              <input
                className="input-form"
                placeholder="Введите пароль"
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
            </p>
            {touched.password && errors.password && (
              <p className="error-massage">{errors.password}</p>
            )}
            <button
              disabled={!isValid && !dirty}
              onClick={handleSubmit}
              type="submit"
              className="button-submit"
              data-testid="button-submit"
            >
              Отправить
            </button>
            <p>
              Первый раз на сайте? - <Link to="/registration">Зарегистрирутесь!</Link>
            </p>
          </div>
        )}
      </Formik>
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    token: state.token.data,
  };
};

export default connect(mapStateToProps)(LoginList);
