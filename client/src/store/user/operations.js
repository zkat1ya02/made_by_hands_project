import axios from 'axios';
import actions from './actions';

const setCustomer = (userData) => (dispatch) => {
  axios.post(`/api/customers`, userData).then((res) => {
    dispatch(actions.setCustomerData(res.data));
    dispatch(actions.setCustomerLoading(false));
  });
};
const setLoginCustomer = (userData) => (dispatch) => {
  axios.post(`/api/customers/login`, userData).then((res) => {
    dispatch(actions.setCustomerData(res.data));
    dispatch(actions.setCustomerLoading(false));
  });
};
const getCustomer = () => (dispatch) => {
  axios.get(`/api/customers/customer`).then((res) => {
    dispatch(actions.setCustomerData(res.data));
    dispatch(actions.setCustomerLoading(false));
  });
};
const upDateCustomer = (userData) => (dispatch) => {
  axios.put(`/api/customers`, userData).then((res) => {
    dispatch(actions.setCustomerData(res.data));
    dispatch(actions.setCustomerLoading(false));
  });
};
const upDatePassCustomer = (passData) => (dispatch) => {
  try {
    axios.put(`/api/customers/password`, passData).then((res) => {
      dispatch(actions.setCustomerDataMsg(res.data.message));
      dispatch(actions.setCustomerData(res.data));
      dispatch(actions.setCustomerLoading(false));
    });
  } catch (e) {
    console.log(e);
  }
};

export default {
  getCustomer,
  upDateCustomer,
  upDatePassCustomer,
  setCustomer,
  setLoginCustomer,
};
