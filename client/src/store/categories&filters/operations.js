import axios from 'axios';
import actions from './actions';

const getAllCategories = () => (dispatch) => {
  axios.get('/api/catalog').then((res) => {
    if (res && res.data) {
      dispatch(actions.setAllCategories(res.data));
    }
  });
};

export default {
  getAllCategories,
};
