import axios from 'axios';
import actions from './actions';

const getComment = (data) => (dispatch) => {
  axios.get(`/api/comments/product/${data}`).then((res) => {
    dispatch(actions.setCommentData(res.data));
    dispatch(actions.setCommentLoading(false));
  });
};
const postComment = (commentItem) => (dispatch) => {
  axios.post(`/api/comments`, commentItem).then((res) => {
    /* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
    const link = res.data.product._id;
    dispatch(getComment(link));
    dispatch(actions.setCommentLoading(false));
  });
};

export default {
  postComment,
  getComment,
};
