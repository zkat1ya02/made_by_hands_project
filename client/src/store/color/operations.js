import axios from 'axios';
import actions from './actions';

const getColor = () => (dispatch) => {
  axios.get(`/api/colors`).then((res) => {
    dispatch(actions.setColorData(res.data));
    dispatch(actions.setColorLoading(false));
  });
};

export default {
  getColor,
};
