import axios from 'axios';
import actions from './actions';

const getSlides = () => (dispatch) => {
  axios.get('api/slides').then((res) => {
    dispatch(actions.getSlidesData(res.data));
    dispatch(actions.getSlidesLoading(false));
  });
};

export default {
  getSlides,
};
