import axios from 'axios';
import actions from './actionsCart';

const postBigDataCart = (data) => (dispatch) => {
  axios.post(`/api/cart`, data).then((res) => {
    dispatch(actions.setCartData(res.data));
    dispatch(actions.setCartLoading(false));
  });
};
const getCart = () => (dispatch) => {
  axios.get(`/api/cart`).then((res) => {
    dispatch(actions.setCartData(res.data));
    dispatch(actions.setCartLoading(false));
  });
};
const postCart = (data) => (dispatch) => {
  axios.put(`/api/cart/${data}`).then((res) => {
    dispatch(actions.setCartData(res.data));
    dispatch(actions.setCartLoading(false));
  });
};
const deleteCart = (data) => (dispatch) => {
  axios.delete(`/api/cart/${data}`).then((res) => {
    dispatch(actions.setCartData(res.data));
    dispatch(actions.setCartLoading(false));
  });
};
const deleteOneItemCart = (data) => (dispatch) => {
  axios.delete(`/api/cart/product/${data}`).then((res) => {
    dispatch(actions.setCartData(res.data));
    dispatch(actions.setCartLoading(false));
  });
};

export default {
  getCart,
  postCart,
  deleteCart,
  deleteOneItemCart,
  postBigDataCart,
};
