import React, { useEffect } from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import ProductDetails from '../pages/ProductDetails/ProductDetails';
import Filter from '../pages/Filter/Filter';
import CartPage from '../pages/Cart/Cart';
import Page404 from '../pages/404/404';
import FavoritesList from '../pages/FavoritesList/FavoritesList';
import RegistrationList from '../pages/RegistrationList/RegistrationList';
import LoginList from '../pages/LoginList/LoginList';
import UserInfo from '../pages/UserInfo/UserInfo';
import { favoritesOperations } from '../store/favorites';
import { cartOperations } from '../store/cart/indexCart';
import { customerOperations } from '../store/user';
import { tokenOperations } from '../store/token';
import { historyOperations } from '../store/history';
import ProductsList from '../components/ProductsList/ProductsList';
import Main from '../pages/Main/Main';

function AppRoutes({ dispatch, token, customer }) {
  useEffect(() => {
    const tokenAuth = JSON.parse(localStorage.getItem('token'));
    if (tokenAuth) {
      axios.defaults.headers.common.Authorization = tokenAuth;
    } else {
      axios.defaults.headers.common.Authorization = null;
    }
    dispatch(tokenOperations.getToken());
    dispatch(favoritesOperations.getFavorites());
    dispatch(historyOperations.getHistory());
    dispatch(customerOperations.getCustomer());
    dispatch(cartOperations.getCart());
  }, [token]);
  console.log(token);
  const isAuth = !!customer.firstName;
  return (
    <Switch>
      <Redirect exact from="/" to="/main" />
      <Route exact path="/main">
        <Main />
      </Route>
      <Route exact path="/shop">
        <Main />
      </Route>
      <Route exact path="/shop/:itemNo">
        <ProductDetails />
      </Route>
      <Route exact path="/filter">
        <Filter />
      </Route>
      <Route exact path="/products">
        <ProductsList />
      </Route>
      <ProtectedRoute exact path="/cart" isAuth={isAuth}>
        <CartPage />
      </ProtectedRoute>
      <Route exact path="/favorites">
        <FavoritesList />
      </Route>
      <Route exact path="/login" isAuth={isAuth}>
        {isAuth ? <UserInfo /> : <LoginList />}
      </Route>
      <ProtectedRoute exact path="/userinfo" isAuth={isAuth}>
        <UserInfo />
      </ProtectedRoute>
      <Route exact path="/registration">
        <RegistrationList />
      </Route>
      <Route path="*">
        <Page404 />
      </Route>
    </Switch>
  );
}
const ProtectedRoute = ({ children, isAuth }) => (
  <Route>
    {isAuth && children}
    {!isAuth && <Redirect to="/login" />}
  </Route>
);
const mapStateToProps = (state) => {
  return {
    token: state.token.data,
    customer: state.customer.data,
  };
};

export default connect(mapStateToProps)(AppRoutes);
