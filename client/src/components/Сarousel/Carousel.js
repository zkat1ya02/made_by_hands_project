/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';
import './Carousel.scss';
import CarouselItem from '../CarouselItem/CarouselItem';
import { slidesOperations } from '../../store/slides';

const Carousel = ({ slides, dispatch }) => {
  useEffect(() => {
    dispatch(slidesOperations.getSlides());
  }, []);
  const slidesItems = slides.map((item) => {
    return <CarouselItem item={item} />;
  });
  const settings = {
    customPaging: (i) => {
      const item = slides[i];
      const img = item.imageUrl;
      return (
        <div className="test">
          <img
            src={img}
            alt="hot offer"
            style={{
              width: '50px',
              height: '50px',
              objectFit: 'cover',
            }}
          />
        </div>
      );
    },
    dots: true,
    dotsClass: 'slick-dots slick-thumb',
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <div className="carousel--wrapper wrapper">
      <h2 className="page__title">Акционные товары</h2>
      <Slider {...settings}> {slidesItems} </Slider>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    slides: state.slides.data,
  };
};

export default connect(mapStateToProps)(Carousel);
