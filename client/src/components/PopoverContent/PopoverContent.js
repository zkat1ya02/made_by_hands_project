import React from 'react';
import { connect } from 'react-redux';
import { CloseCircleOutlined } from '@ant-design/icons';
import { message } from 'antd';
import 'antd/dist/antd.css';
import './PopoverContent.css';
import { favoritesOperations } from '../../store/favorites';

const PopoverContent = ({ name, dispatch }) => {
  const favoriteBtnAction = () => {
    const artCollection = [...(JSON.parse(localStorage.getItem('favorites')) || '')];
    if (artCollection.find((item) => item.name === name)) {
      const collection = artCollection.filter((item) => {
        return item.name !== name;
      });
      localStorage.setItem('favorites', JSON.stringify(collection));
      message.success(`Товар ${name} удален из избранного! А жаль!`, 2);
    }
    dispatch(favoritesOperations.getFavorites());
  };
  return (
    <div className="popover-content">
      {name}
      <CloseCircleOutlined onClick={favoriteBtnAction} />
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    favorites: state.favorites.data,
  };
};

export default connect(mapStateToProps)(PopoverContent);
