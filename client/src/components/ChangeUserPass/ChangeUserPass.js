import React from 'react';
import axios from 'axios';
import * as yup from 'yup';
import { Formik } from 'formik';
import { message } from 'antd';
import { useHistory } from 'react-router';

const ChangeUserPass = () => {
  const hist = useHistory();
  const changePass = async (data) => {
    const body = JSON.stringify(data);
    console.log(body);
    await axios({
      method: 'put',
      url: `/api/customers/password`,
      headers: { 'Content-Type': 'application/json' },
      data: body,
    })
      .then(function (response) {
        if (response.data.password) message.error(`${response.data.password}`, 2);
        if (response.data.message) {
          message.success(`${response.data.message}`, 2);
          hist.push('/userinfo');
        }
      })
      .catch(function (error) {
        console.log(error);
        message.error('Что-то пошло не так!', 2);
      });
  };
  const validationSchema = yup.object().shape({
    password: yup
      .string()
      .min(8, 'Password is too short - should be 8 chars minimum.')
      .matches(/[a-zA-Z]/, 'Password can only contain Latin letters.')
      .required('Поле обязательно для ввода'),
    newPassword: yup
      .string()
      .min(8, 'Password is too short - should be 8 chars minimum.')
      .matches(/[a-zA-Z]/, 'Password can only contain Latin letters.')
      .required('Поле обязательно для ввода'),
  });
  return (
    <div className="form-wrapper">
      <Formik
        initialValues={{
          password: '',
          newPassword: '',
        }}
        validateOnBlur
        onSubmit={(values) => {
          changePass(values);
        }}
        validationSchema={validationSchema}
      >
        {({ values, errors, touched, handleChange, handleBlur, isValid, handleSubmit, dirty }) => (
          <div className="form-conteiner">
            <p className="title">Изменение пароля</p>
            <p>
              <input
                className="input-form"
                placeholder="Введите старый пароль"
                type="text"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
            </p>
            {touched.password && errors.password && (
              <p className="error-massage">{errors.password}</p>
            )}
            <p>
              <input
                className="input-form"
                placeholder="Введите новый пароль"
                type="text"
                name="newPassword"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.newPassword}
              />
            </p>
            {touched.newPassword && errors.newPassword && (
              <p className="error-massage">{errors.newPassword}</p>
            )}
            <button
              disabled={!isValid && !dirty}
              onClick={handleSubmit}
              type="submit"
              className="button-submit"
              data-testid="button-submit"
            >
              Отправить
            </button>
          </div>
        )}
      </Formik>
    </div>
  );
};
export default ChangeUserPass;
