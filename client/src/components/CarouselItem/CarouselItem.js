import React from 'react';
import './CarouselItem.scss';
import { Popover } from 'antd';

const CarouselItem = ({ item }) => {
  const { itemNo, name, imageUrl, description } = item;
  const expandItem = `/shop/${itemNo}`;
  const historyBtnAction = () => {
    const artCollection = [...(JSON.parse(localStorage.getItem('history')) || '')];
    if (!artCollection.find((itemData) => itemData.itemNo === itemNo)) {
      artCollection.push(item);
      if (artCollection.length > 5) artCollection.splice(0, 1);
      localStorage.setItem('history', JSON.stringify(artCollection));
    }
  };
  return (
    <a href={expandItem} onClick={historyBtnAction}>
      <div className="carousel-item__block">
        <div className="carousel-item__wrapper">
          <Popover title={name} content={description} trigger="hover" placement="right">
            <img className="carousel-item__img" src={imageUrl} alt="hot offer" />
          </Popover>
        </div>
      </div>
    </a>
  );
};

export default CarouselItem;
