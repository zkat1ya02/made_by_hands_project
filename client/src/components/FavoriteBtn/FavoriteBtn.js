import React from 'react';
import { connect } from 'react-redux';
import 'antd/dist/antd.css';
import { HeartOutlined } from '@ant-design/icons';
import { message, Button } from 'antd';
import { favoritesOperations } from '../../store/favorites';

const FavoriteBtn = ({ itemData, itemNo, btnText, setBtnText, favorites, dispatch }) => {
  setBtnText(
    favorites.find((item) => item.itemNo === itemNo)
      ? 'Удалить из избранного'
      : 'Добавить в избранное'
  );
  const favoriteBtnAction = () => {
    const artCollection = [...(JSON.parse(localStorage.getItem('favorites')) || '')];
    if (artCollection.find((item) => item.itemNo === itemNo)) {
      const collection = artCollection.filter((item) => {
        return item.itemNo !== itemData.itemNo;
      });
      localStorage.setItem('favorites', JSON.stringify(collection));
      message.success('Товар удален из избранного! А жаль!', 2);
    }
    if (!artCollection.find((item) => item.itemNo === itemNo)) {
      artCollection.push(itemData);
      localStorage.setItem('favorites', JSON.stringify(artCollection));
      message.success('Товар добавлен в избранное. Спасибо!', 2);
    }
    setBtnText(
      favorites.find((item) => item.itemNo === itemNo)
        ? 'Удалить из избранного'
        : 'Добавить в избранное'
    );
    dispatch(favoritesOperations.getFavorites());
  };
  return (
    <div>
      <Button type="primary" shape="round" icon={<HeartOutlined />} onClick={favoriteBtnAction}>
        {btnText}
      </Button>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    favorites: state.favorites.data,
  };
};

export default connect(mapStateToProps)(FavoriteBtn);
