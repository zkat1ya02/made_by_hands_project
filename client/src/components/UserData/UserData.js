import React from 'react';
import { connect } from 'react-redux';

const UserData = ({ customer }) => {
  const { firstName, lastName, email, login, telephone, date } = customer;
  return (
    <div className="form-wrapper">
      <div className="form-conteiner">
        <h3 className="title">Данные пользователя</h3>
        <p>Имя {firstName}</p>
        <p>Фамилия {lastName}</p>
        <p>Email {email}</p>
        <p>Номер телефона {telephone}</p>
        <p>Login {login}</p>
        <p>Дата регистрации пользоватея {date.substring(0, 10)}</p>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    customer: state.customer.data,
    isLoading: state.customer.isLoading,
  };
};

export default connect(mapStateToProps)(UserData);
